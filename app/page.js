import Head from 'next/head'
import Image from 'next/image'

export default function Home() {
  return (
    <main>
      <header>
        <nav className='flex justify-between font-bold'>
          <div className='w-[30%] flex flex-col justify-start items-start p-2'>
            <h1 className='text-md'>Work hours</h1>
            <div className='flex'>
            <img src="./assets/icons/clock.svg" className='h-8 pe-1' alt="clock" />
                <p className='text-xs tracking-tight'>9:30am <br/> 7:00pm</p>
            </div>
          </div>
          <img src="./assets/icons/Logo.png" className='h-[54px] my-2' alt="" />
          <div className='w-[30%] flex flex-col text-right justify-end items-end p-2'>
            <h1 className='text-sm'>Follow <br/> us</h1>
            <img src="./assets/icons/points.png" className='h-4' alt="" />
          </div>
        </nav>

        <img src="./assets/wildencounters.png" className='pt-2' alt="banner" />
      </header>
      <section className='flex justify-center text-xl py-4 font-semibold'>
        <h1 className='uppercase text-center'>Let your curiosity <br/> come alive</h1>
      </section>
    </main>
  )
}
